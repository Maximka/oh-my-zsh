# This file integrates the history-substring-search script into oh-my-zsh.

if lsb_release -a 2> /dev/null | grep Ubuntu >/dev/null 2>&1
    then
    UBUNTU=1
else
    UBUNTU=0
fi

if [[ $UBUNTU == 1 ]]
    then
    WRITE_ENV=1
    if [ -f $HOME/.zshenv ]
    then
        if cat $HOME/.zshenv | grep DEBIAN_PREVENT_KEYBOARD_CHANGES &> /dev/null
        then
            WRITE_ENV=0
        fi
    fi
    if [[ $WRITE_ENV == 1 ]]
        then
        echo "DEBIAN_PREVENT_KEYBOARD_CHANGES=yes" >> $HOME/.zshenv
    fi
fi

source "$ZSH/plugins/history-substring-search/history-substring-search.zsh"

if test "$CASE_SENSITIVE" = true; then
  unset HISTORY_SUBSTRING_SEARCH_GLOBBING_FLAGS
fi

if test "$DISABLE_COLOR" = true; then
  unset HISTORY_SUBSTRING_SEARCH_HIGHLIGHT_FOUND
  unset HISTORY_SUBSTRING_SEARCH_HIGHLIGHT_NOT_FOUND
fi
